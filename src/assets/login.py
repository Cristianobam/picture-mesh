import streamlit as st
import requests
import json
    
def login():
    if st.session_state.auth_status == True:
        st.sidebar.success("Você já está conectado")
    else:
        st.sidebar.title("Login")
        form = st.sidebar.form('Login-Form', clear_on_submit=True)
        email = form.text_input('E-mail')
        button = form.form_submit_button('Enviar')
        if button:
            verifyCredetials(email=email)
        
def verifyCredetials(email):
    email = email.lower().strip()
    url = "https://apidev.provadorvirtual.app.br/admin/login"

    payload = json.dumps({
    "email": email,
    "secureKey": "1X2Y3Z"
    })
    headers = {
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload, timeout=3)
    if response.status_code == 200:
        st.session_state.auth_status = True
        st.session_state.auth_response = eval(response.text\
                                        .replace("true", "True")\
                                        .replace("false","False"))
    else:
        st.session_state.auth_status = False
        st.sidebar.error("😕 Verifique o E-Mail inserido")
            