import cv2
import json
import base64
import requests

from trimesh.viewer import scene_to_html

def normalize_image(img, height, xpadding=230):
    (h, w) = img.shape[:2]
    window_height = height
    new_width = int(float(window_height)*float(w)/float(h))
    img = cv2.resize(img, (new_width, window_height), interpolation=cv2.INTER_LINEAR)
    img = cv2.copyMakeBorder(img, 0, 0, xpadding, xpadding, cv2.BORDER_CONSTANT, value=(14, 17, 23))
    return img

def render_image(image):
    """Renders the given svg string."""
    return f'<img style="width:100%; padding: 0 20%;" src="data:image/png;base64,{base64.b64encode(open(image, "rb").read()).decode()}">'

def scene_to_iframe(scene, height=500, **kwargs):
    as_html = scene_to_html(scene=scene).replace('scene.background=new THREE.Color(0xffffff)', 'scene.background=new THREE.Color(0x0e1117)')
    as_html.replace('controls.noPan=false', 'controls.noPan=true')
    srcdoc = as_html.replace('"', '&quot;')
    embedded = ' '.join([
        '<iframe srcdoc="{srcdoc}"',
        'width="100%" height="{height}px"',
        'style="border:none;"></iframe>']).format(
            srcdoc=srcdoc,
            height=height)
    return embedded

def post2server(data, id):
    url = f'https://apidev.provadorvirtual.app.br/measurements/{id}'

    payload= json.dumps({'measurements':data})
    headers = {'Content-Type': 'application/json'}
    response = requests.request('POST', url, headers=headers, data=payload, timeout=10)
    return response