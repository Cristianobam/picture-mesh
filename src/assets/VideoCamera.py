import cv2
import streamlit as st
import imutils

class VideoCamera(object):
    def __init__(self, index:int=0):
        self.video = cv2.VideoCapture(index)
        self.isAvailable = True
        self.frame = None
    
    def turnoff(self):
        self.isAvailable = False
        self.video.release() # Liberar a webCam
        
    def get_frame(self, rotate=-90):
        _, frame = self.video.read()
        frame = imutils.rotate_bound(cv2.flip(frame, 1), rotate)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        self.frame = frame.copy()
        return frame