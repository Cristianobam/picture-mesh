#%%
import sys
sys.path.insert(0, '..')

import os
import cv2

from datetime import datetime
from src.mesh_generator import MeshGenerator
from src.mesh_evaluation import mesh_evaluation
from src.image_processing import EvalImage

#%%
DEVICE = 'cpu' # Default NN device
RESOLUTION = 300 # Mesh resolution. Greater resolution implies more memory on cuda device.
MODEL_PATH = f'..{os.sep}models{os.sep}pifuhd.pt' # path to model
timeInit = datetime.now() # Get initial time

meshGenerator = MeshGenerator(device=DEVICE, resolution=RESOLUTION, checkpoint=MODEL_PATH) # Initialize NN
# %%
IMAGE_PATH = '../sample_images/bla.jpg'
img = cv2.imread(IMAGE_PATH, cv2.IMREAD_COLOR)
img_info = EvalImage(img)

# %%
mesh = meshGenerator(img_info)
name = datetime.now().strftime('%Y%m%d-%H%M%S')
mesh.export(f'..{os.sep}results{os.sep}{name}.obj')