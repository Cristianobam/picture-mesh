import sys
sys.path.insert(0, '..')
import os
import cv2
import json
import torch
import codecs
import pickle
import trimesh
import pandas as pd
import streamlit as st

from PIL import Image
from datetime import datetime
from assets.login import login
from generator import recon_from_array
from assets.VideoCamera import VideoCamera
from assets.utils import normalize_image, render_image, scene_to_iframe, post2server
from streamlit_lottie import st_lottie, st_lottie_spinner

CAMERA_ANGLE = -90

try: os.mkdir(f'..{os.sep}/results')
except: pass

st.set_page_config(
    page_title="Provador Virtual",
    page_icon=Image.open(f"assets{os.sep}logo.png"),
    layout="centered")

hide_menu_style = "<style>#MainMenu {visibility: hidden;} footer {visibility: hidden;}</style>"
st.markdown(hide_menu_style, unsafe_allow_html=True) 
st.markdown("<style>div.stButton > button:first-child {width:100%;border-radius:10px 10px 10px 10px;}</style>", unsafe_allow_html=True)

if 'auth_status' not in st.session_state:
    st.session_state.auth_status = False

if 'camera' not in st.session_state:
    st.session_state.camera = True

if 'image' not in st.session_state:
    st.session_state.image = None

if 'data_confirm' not in st.session_state:
    st.session_state.data_confirm = False
    
if 'mesh_recon' not in st.session_state:
    st.session_state.mesh_recon = None
    
if 'mesh_recon_result' not in st.session_state:
    st.session_state.mesh_recon_result = None

lottie_json = json.load(open(f"assets{os.sep}scanner.json", 'r'))
scanner_json = json.load(open(f"assets{os.sep}tela-inicio.json", 'r'))
camera_json = json.load(open(f"assets{os.sep}camera.json", 'r'))
placeholder = st.empty()

def load_data():
    response = st.session_state.auth_response
    st.session_state.height = response['user']['height']
    df = pd.DataFrame(columns=response['user'].keys(), data=[response['user'].values()])[['firstName', 'lastName', 'gender', 'age', 'height', 'weight']]
    df.columns = ['Primeiro Nome', 'Sobrenome', 'Sexo', 'Idade', 'Altura', 'Peso']
    st.title('Informações')
    st.markdown(f"""<table style="width:100%;margin:0 0 10% 0;border:none"><thead><tr>
    <th>Primeiro Nome</th><th>Sobrenome</th></tr></thead><tbody>
    <tr><td>{response['user']['firstName']}</td><td>{response['user']['lastName']}</td></tr>
    </tbody></table>""", unsafe_allow_html=True)
    
    st.markdown(f"""<table style="width:100%;margin:0 0 10% 0;"><tbody>
    <tr><td>Idade:</td><td>{response['user']['age']}</td></tr>
    <tr><td>Sexo:</td><td>{response['user']['gender']}</td></tr>
    <tr><td>Altura:</td><td>{response['user']['height']}</td></tr>
    <tr><td>Peso:</td><td>{response['user']['weight']}</td></tr></tbody></table>""", unsafe_allow_html=True)
    
def data_confirm():
    st.session_state.data_confirm = True

def run_mesh_recon():
    st.session_state.name = f'..{os.sep}results{os.sep}{datetime.now().strftime("%Y%m%d-%H%M%S")}'
    cv2.imwrite(st.session_state.name + '.jpg', cv2.cvtColor(st.session_state.image, cv2.COLOR_RGB2BGR))
    st.session_state.mesh_recon = True

def return_camera():
    st.session_state.image = None
    st.session_state.mesh_recon = None
    st.session_state.camera = True

def main():
    placeholder.empty()
    with placeholder.container():
        st_lottie(scanner_json, key='Initial')
    with st.sidebar:
        st.markdown(render_image(f'assets{os.sep}logo.png'), unsafe_allow_html=True)
    login()
    if st.session_state.auth_status:
        placeholder.empty()
        if st.session_state.data_confirm == False:
            with placeholder.container():
                load_data()
                _, col2 = st.columns([20,3])
                with col2:
                    st.button('Confirmar', on_click=data_confirm)
        
        else:
            if st.session_state.camera:
                st.session_state.camera = False
                camera = VideoCamera()
                
                placeholder.empty()
                with placeholder.container():
                    col1, col2 = st.columns([3,1])
                    with col1:
                        FRAME_WINDOW = st.image([])
                    with col2:
                        for _ in range(10): st.write('')
                        st_lottie(camera_json)
                        button = st.button('Tirar Foto', on_click=camera.turnoff)
                    while not button:
                        img = camera.get_frame(rotate=CAMERA_ANGLE)
                        while img is None:
                            img = camera.get_frame(rotate=CAMERA_ANGLE)
                        st.session_state.image = img.copy()
                        FRAME_WINDOW.image(normalize_image(img, 700, 100))
            
        if st.session_state.image is not None:
            placeholder.empty()
            with placeholder.container():
                st.write('Imagem')      
                st.image(normalize_image(st.session_state.image, 700))
                col1, _, col2 = st.columns([4,20,4])
                with col1:
                    st.button('Voltar', on_click=return_camera)
                    
                with col2:
                    st.button('Confirmar', on_click=run_mesh_recon)
        
        if st.session_state.mesh_recon is not None:
            if st.session_state.mesh_recon_result is None:
                placeholder.empty()
                with st_lottie_spinner(lottie_json, key='Final'):
                    obj_base64string = codecs.encode(pickle.dumps(st.session_state.image, protocol=pickle.HIGHEST_PROTOCOL), "base64").decode('latin1')
                    cmd = ['--input_image', obj_base64string, '--load_netMR_checkpoint_path', f'..{os.sep}models{os.sep}pifuhd.pt',
                           '--output_path', f'..{os.sep}results', '--save_all', 'True', '--output_name', st.session_state.name.split('/')[-1]]
                    st.session_state.mesh_recon_result = recon_from_array(cmd)
                    if torch.cuda.is_available(): torch.cuda.empty_cache()
                    
            if st.session_state.mesh_recon_result is not None:
                placeholder.empty()
                with placeholder.container():
                    response = post2server(data=st.session_state.mesh_recon_result, id=st.session_state.auth_response['user']['id'])
                    if response.status_code == 200:
                        st.success('Sucesso! A malha foi gerada e os dados estão nos servidor.')
                    else:
                        st.warning('Não foi possível concetar-se ao servidor. Tente novamente mais tarde.')
                    loaded_mesh = trimesh.load(st.session_state.name + '.obj')
                    st.markdown(scene_to_iframe(loaded_mesh.scene()), unsafe_allow_html=True)
                
if __name__ == '__main__':
    main()