import sys
sys.path.insert(0, '..')
import os
import torch
import trimesh
import numpy as np

from mesh.utils import drop_artefacts
from src.image_processing import EvalImage
from lib.model import HGPIFuNetwNML, HGPIFuMRNet
from lib.mesh_util import string_obj_mesh, reconstruction

CHECKPOINT_PATH = f'..{os.sep}models{os.sep}pifuhd.pt'

class MeshGenerator:
    def __init__(self, device='cpu', resolution=300, checkpoint=CHECKPOINT_PATH, loadSize=1024):
        self.device = device
        self.resolution = resolution
        self.loadSize = loadSize
        self.state_dict = torch.load(checkpoint, map_location=self.device)
        self.opt = self.state_dict['opt']
        
        self.netG = HGPIFuNetwNML(self.state_dict['opt_netG'], projection_mode='orthogonal').to(device=self.device)
        self.netMR = HGPIFuMRNet(self.opt, self.netG, projection_mode='orthogonal').to(device=self.device)
        self.netMR.load_state_dict(self.state_dict['model_state_dict'])
        
    def __call__(self, image):
        with torch.no_grad():       
            self.netG.eval()
            mesh_string = self.__gen_mesh(self.resolution, self.netMR, self.device, image.dict, components=self.opt.use_compose)
        if torch.cuda.is_available(): torch.cuda.empty_cache()
        loaded_mesh = trimesh.load(file_obj = trimesh.util.wrap_as_stream(mesh_string), file_type='obj')
        loaded_mesh = drop_artefacts(loaded_mesh)
        return loaded_mesh
        
    def __gen_mesh(self, res, net, device, data, thresh=0.5, use_octree=True, components=False):
        image_tensor_global = data['img_512'].to(device=device)
        image_tensor = data['img'].to(device=device)
        calib_tensor = data['calib'].to(device=device)

        net.filter_global(image_tensor_global)
        net.filter_local(image_tensor[:,None])

        try:
            if net.netG.netF is not None:
                image_tensor_global = torch.cat([image_tensor_global, net.netG.nmlF], 0)
            if net.netG.netB is not None:
                image_tensor_global = torch.cat([image_tensor_global, net.netG.nmlB], 0)
        except:
            pass
        
        try:
            verts, faces, _, _ = reconstruction(
                net, device, calib_tensor, res, thresh, use_octree=use_octree, num_samples=50000)
            verts_tensor = torch.from_numpy(verts.T).unsqueeze(0).to(device=device).float()

            color = np.zeros(verts.shape)
            interval = 50000
            for i in range(len(color) // interval + 1):
                left = i * interval
                if i == len(color) // interval:
                    right = -1
                else:
                    right = (i + 1) * interval
                net.calc_normal(verts_tensor[:, None, :, left:right], calib_tensor[:,None], calib_tensor)
                nml = net.nmls.detach().cpu().numpy()[0] * 0.5 + 0.5
                color[left:right] = nml.T

            return string_obj_mesh(verts, faces, color)
        except Exception as e:
            print(e)