#%%
import sys
sys.path.insert(0, '..')

import gc
import os
import json
import torch
import logging
import requests
import shutil 
import cv2

from datetime import datetime
from src.image_processing import EvalImage
from src.mesh_generator import MeshGenerator
from src.mesh_evaluation import mesh_evaluation
from loggingext.colargulog import ColorizedArgsFormatter
from loggingext.colargulog import BraceFormatStyleFormatter

#%%
DEBUG = True
FIRST_EXEC = True
TIMEOUT = 3 # Default timeout
IRI = 5*60 # Inter runs interval
URL = 'https://apidev.provadorvirtual.app.br/measurements/waiting' # API url
DEVICE = 'cpu' # Default NN device
RESOLUTION = 200 # Mesh resolution. Greater resolution implies more memory on cuda device.
MODEL_PATH = f'..{os.sep}..{os.sep}models{os.sep}pifuhd.pt' # path to model
timeInit = datetime.now() # Get initial time

if sys.platform.startswith('linux'):
    IRI = 10*60 # Inter runs interval
    DEVICE = 'cuda' # Default NN device
    RESOLUTION = 200 # Mesh resolution. Greater resolution implies more memory on cuda device.
    MODEL_PATH = f'..{os.sep}models{os.sep}pifuhd.pt' # path to model

meshGenerator = MeshGenerator(device="cpu", resolution=RESOLUTION, checkpoint=MODEL_PATH) # Initialize NN

#%% Setup Logging Options
root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)

console_level = "INFO"
console_handler = logging.StreamHandler(stream=sys.stdout)
console_handler.setLevel(console_level)
console_format = "%(asctime)s - %(levelname)-8s - %(message)s"
colored_formatter = ColorizedArgsFormatter(console_format)
console_handler.setFormatter(colored_formatter)
root_logger.addHandler(console_handler)

file_handler = logging.FileHandler("local_processing.log")
file_level = "DEBUG"
file_handler.setLevel(file_level)
file_format = "%(asctime)s - %(levelname)-8s - %(message)s"
file_handler.setFormatter(BraceFormatStyleFormatter(file_format))
root_logger.addHandler(file_handler)

#%% Define server communication
def post2server(data, id):
    url = f'https://apidev.provadorvirtual.app.br/measurements/{id}'
    payload= json.dumps({'measurements': data})
    headers = {'Content-Type': 'application/json'}
    response = requests.request('POST', url, headers=headers, data=payload, timeout=60)
    return response

def download_image(image_url, filename):
    r = requests.get(image_url, stream=True)
    if r.status_code == 200:
        r.raw.decode_content = True
        with open(filename,'wb') as f:
            shutil.copyfileobj(r.raw, f)
    else:
        raise "There was an error while downlading the image"

#%%
if __name__ == '__main__':
    try: os.mkdir('../results')
    except: pass
    logger = logging.getLogger(__name__)
    logger.info("Inicializando...")

    while True:
        if ((datetime.now() - timeInit).seconds > IRI) or FIRST_EXEC:
            FIRST_EXEC = False
            logger.info("Retrieving pending users")
            timeInit = datetime.now()
            response = requests.request('GET', url=URL, timeout=TIMEOUT)
            if response.status_code != 200:
                logger.error("There was an error while trying to retrieve pending users.")
                continue
            
            for user in json.loads(response.text):
                gc.collect()
                if torch.cuda.is_available: torch.cuda.empty_cache()
                try: logger.info("Initializing user data retrieval for {}", user['firstName'])
                except: logger.info("Initializing user data retrieval")

                if 'photos' not in user:
                    try: logger.error("There was an error while trying to retrieve data from {}.", user['firstName'])
                    except: logger.error("There was an error while trying to retrieve data")
                    continue
                
                output_path = f'..{os.sep}results{os.sep}{user["id"]}'
                try: os.mkdir(output_path)
                except: pass
                
                try:
                    download_image(user['photos']['front'], f'{output_path}{os.sep}downloaded_image.jpg')
                    img = cv2.imread(f'{output_path}{os.sep}downloaded_image.jpg', cv2.IMREAD_COLOR)
                except:
                    try: logger.error("There was an error while trying to retrieve photo from {}.", user['firstName'])
                    except: logger.error("There was an error while trying to retrieve photo")
                    continue
                
                try: img_info = EvalImage(img)
                except:
                    logger.critical("There was an error while trying to crop picture. Make sure to take a clear picture.")
                    continue
                    
                try:
                    mesh = meshGenerator(img_info)
                    logger.info("Body mesh creation concluded")
                except:
                    logger.critical("There was an error while trying to create the mesh...")
                    continue

                if torch.cuda.is_available: torch.cuda.empty_cache()
                try:
                    info, filename = mesh_evaluation(mesh, user['height'], user['gender'], user['age'], user['weight'], output_path=output_path, debug=DEBUG)
                    logger.info("Body assessment concluded")
                except:
                    logger.critical("There was an error while trying to assess body mesh")
                    continue
                
                del mesh

                if not DEBUG:
                    response = post2server(info, user['id'])
                    if response.status_code == 200: 
                        logger.info("Measurements posted to server")
                        try: cv2.imwrite(f'{output_path}{os.sep}{filename}_baseline.jpg', img)
                        except:
                            logger.error("There was an error while trying to save baseline image")
                    else: logger.error("Failed to post measurements to server. Error {}", response.status_code)