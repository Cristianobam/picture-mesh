import trimesh
import numpy as np
from mediapipe.python.solutions import holistic as mp_holistic
from networkx import from_edgelist

class Articulations(object):
    def __init__(self):
        self.landmarks = ['NOSE', 'LEFT_SHOULDER', 'RIGHT_SHOULDER',
                      'LEFT_ELBOW', 'RIGHT_ELBOW', 'LEFT_WRIST', 'RIGHT_WRIST',
                      'LEFT_HIP', 'RIGHT_HIP', 'LEFT_KNEE', 'RIGHT_KNEE',
                      'LEFT_ANKLE', 'RIGHT_ANKLE']

    def __repr__(self):
        return str(self.landmarks)

    def __getitem__(self, index):
        return self.landmarks[index]

    def __len__(self):
        return len(self.landmarks)

class Base(object):
    def __init__(self, mesh, image):
        self.mesh = mesh
        self.proximity = trimesh.proximity.ProximityQuery(self.mesh)    
        self.landmarks = Articulations()

        self.image = image
        self.image_height, _, _ = self.image.shape

        self.wg = self.__weighted_graph()
        self.position = {}
        self.results = None
        self.__get_articulations()
        self.__get_3D_pos()

    def __weighted_graph(self):
        edges = self.mesh.edges_unique
        length = self.mesh.edges_unique_length
        return from_edgelist([(e[0], e[1], {'length': L}) for e, L in zip(edges, length)])

    def __get_articulations(self):
        with mp_holistic.Holistic(static_image_mode=True, model_complexity=2,
                            enable_segmentation=True, refine_face_landmarks=True,
                            min_detection_confidence=.85) as pose:

            self.results = pose.process(self.image)
            
            condition = np.stack((self.results.segmentation_mask,) * 3, axis=-1) > 0.1
            mask = condition.mean(axis=-1)
            rows = np.max(mask, axis=1)
            row_low = int(rows.argmax())
            row_high = int(-rows[::-1].argmax())
            row_high = -1 if row_high == 0 else row_high
            self.height2pixels = (self.image_height-row_high+1) - row_low

    def __getitem__(self, index):
        return self.__get_2D_pos(self.landmarks[index])

    def __get_2D_pos(self, landmark):
        x = (self.results.pose_landmarks.landmark[mp_holistic.PoseLandmark[landmark]].x - 1/2) * self.image_height * self.image2mesh
        y = (1/2 - self.results.pose_landmarks.landmark[mp_holistic.PoseLandmark[landmark]].y) * self.image_height * self.image2mesh
        return np.array([x, y, 0])
    
    def __get_3D_pos(self):
        for landmark in self.landmarks:
            pos =  self.__get_2D_pos(landmark)
            pos[-1] = np.max(self.mesh.bounds) * 2
            loc = self.mesh.ray.intersects_location(ray_origins=[pos], ray_directions=[[0,0,-1]])[0]
            if ('RIGHT' in landmark) and (len(loc) == 0):
                slicing = list()
                for i in range(1000):
                    loc = self.mesh.ray.intersects_location(ray_origins=[[pos[0]-i*0.001, pos[1], pos[2]]], ray_directions=[[0,0,-1]])[0]
                    slicing.append(np.nanmean(loc, axis=0) if len(loc) > 0 else np.ones(3) * np.nan)
                
                center = np.nanmean(slicing, axis=0)
                if any(np.isnan(center)):
                    for i in range(1000):
                        loc = self.mesh.ray.intersects_location(ray_origins=[[pos[0]+i*0.001, pos[1], pos[2]]], ray_directions=[[0,0,-1]])[0]
                        if len(loc) > 0:
                            center = np.nanmean(loc, axis=0)
                            break
            
            elif ('LEFT' in landmark) and (len(loc) == 0):
                slicing = list()
                for i in range(1000):
                    loc = self.mesh.ray.intersects_location(ray_origins=[[pos[0]+i*0.001, pos[1], pos[2]]], ray_directions=[[0,0,-1]])[0]
                    slicing.append(np.nanmean(loc, axis=0) if len(loc) > 0 else np.ones(3) * np.nan)
                
                center = np.nanmean(slicing, axis=0)
                if any(np.isnan(center)):
                    for i in range(1000):
                        loc = self.mesh.ray.intersects_location(ray_origins=[[pos[0]-i*0.001, pos[1], pos[2]]], ray_directions=[[0,0,-1]])[0]
                        if len(loc) > 0:
                            center = np.nanmean(loc, axis=0)
                            break
            else:
                center = np.nanmean(loc, axis=0) if len(loc) > 0 else None
            
            self.position[landmark] = center

    @property
    def head_height(self):
        if self.results.face_landmarks is None:
            ls = self.position['LEFT_SHOULDER']
            rs = self.position['RIGHT_SHOULDER']
            shoulder = np.nanmean([ls, rs], axis=0)
            nose = self.position['NOSE']
            return shoulder[1] * 1.3 if nose is None else np.mean([nose,shoulder], axis=0)[1]
        return (1/2 - np.max([pos.y for pos in self.results.face_landmarks.landmark])) * self.image_height * self.image2mesh

    @property
    def image2mesh(self):
        return np.diff(self.mesh.bounds, axis=0)[0][1]/self.height2pixels