import trimesh
import numpy as np
from mesh.pose_estimation import Base
from mesh.utils import select_closest
from networkx import shortest_path

class Wardrobe(Base):
    def __init__(self, mesh, image, height):
        super().__init__(mesh, image)
        self.mesh2height = height/np.diff(self.mesh.bounds, axis=0)[0][1]
        self.__initialize()
    
    def apply_scale(func):
        def wrapper(self, **kwargs):
            return func(self, **kwargs) * self.mesh2height
        return wrapper
    
    def __initialize(self):
        self.neck_girth = self.__get_neck_girth()
        self.soulder_length = self.__get_soulder_length()
        self.bust_girth = self.__get_bust_girth()
        self.waist_girth = self.__get_waist_girth()
        self.hip_girth = self.__get_hip_girth()
        self.arm_length = self.__get_arm_length()
        self.front_length = self.__get_trunk_length(isBack=False)
        self.front_width = self.__get_trunk_width(isBack=False)
        self.back_length = self.__get_trunk_length(isBack=True)
        self.back_width = self.__get_trunk_width(isBack=True)
        self.waist_to_hip = self.__get_waist_to_hip()
        self.waist_to_knee = self.__get_waist_to_knee()
        self.waist_to_ankle = self.__get_waist_to_ankle()
        self.hip_to_ankle = self.__get_hip_to_ankle()
        self.bicep_girth = self.__get_bicep_girth()
        self.elbow_girth = self.__get_elbow_girth()
        self.wrist_girth = self.__get_wrist_girth()
        self.knee_girth = self.__get_knee_girth()
        self.calf_girth = self.__get_calf_girth()
        self.ankle_girth = self.__get_ankle_girth()

    @apply_scale
    def __get_neck_girth(self):
        ls = self.position['LEFT_SHOULDER']
        rs = self.position['RIGHT_SHOULDER']
        shoulders = np.mean([ls, rs], axis=0)

        head_y = self.head_height
        shoulders[1] = 0.75*head_y+0.25*shoulders[1]

        mslice = self.mesh.section(plane_origin=shoulders, plane_normal=[0,1,0])
        self.position['NECK'] = mslice.centroid
        return mslice.to_planar()[0].length

    @apply_scale
    def __get_soulder_length(self):
        shoulder_legth = 0
        for side in ['LEFT', 'RIGHT']:
            neck = self.position['NECK'].copy()
            neck[0] *= 1.1
            
            shoulder = self.position[f'{side}_SHOULDER'].copy()
            shoulder[0] *= 1.1
            
            start, end = self.proximity.vertex([neck,shoulder])[-1]
            shoulder_legth += 0.5 * np.linalg.norm(self.mesh.vertices[end]-self.mesh.vertices[start])
        return shoulder_legth
    
    @apply_scale
    def __get_bust_girth(self):
        ls = self.position['LEFT_SHOULDER']
        rs = self.position['RIGHT_SHOULDER']
        shoulders = np.mean([ls, rs], axis=0)

        lh = self.position['LEFT_HIP']
        rh = self.position['RIGHT_HIP']
        hip = np.mean([lh, rh], axis=0)

        dist = np.linalg.norm(shoulders-hip)
        norm = (shoulders-hip)/dist
        steps = np.linspace(0, dist, 1000)

        mslices = self.mesh.section_multiplane(plane_origin=hip, plane_normal=norm, heights=steps)
        mask = np.array([len(mslice.entities) > 2 for mslice in mslices])
        mslices = [select_closest(mslice, np.array([0,0])) for mslice in np.array(mslices)[mask]]
        idx_max = np.argmax([mslice.length for mslice in mslices])
        self.position['BUST'] = hip + norm*steps[mask][idx_max]
        return mslices[idx_max].length
    
    @apply_scale
    def __get_waist_girth(self):
        ls = self.position['LEFT_SHOULDER']
        rs = self.position['RIGHT_SHOULDER']
        shoulders = np.mean([ls, rs], axis=0)

        lh = self.position['LEFT_HIP']
        rh = self.position['RIGHT_HIP']
        hip = np.mean([lh, rh], axis=0)

        dist = np.linalg.norm(shoulders-hip)
        norm = (shoulders-hip)/dist
        steps = np.linspace(0, dist, 1000)
        mslices = self.mesh.section_multiplane(plane_origin=hip, plane_normal=norm, heights=steps)
        mask = np.array([len(mslice.entities) > 2 for mslice in mslices])
        mslices = [select_closest(mslice, np.array([0,0])) for mslice in np.array(mslices)[mask]]
        idx_min = np.argmin([mslice.length for mslice in mslices])
        self.position['WAIST'] = hip + norm * steps[mask][idx_min]
        return mslices[idx_min].length

    @apply_scale
    def __get_hip_girth(self):
        ls = self.position['LEFT_SHOULDER']
        rs = self.position['RIGHT_SHOULDER']
        shoulders = np.mean([ls, rs], axis=0)

        lh = self.position['LEFT_HIP']
        rh = self.position['RIGHT_HIP']
        hip = np.mean([lh, rh], axis=0)

        norm = (shoulders-hip)/np.linalg.norm(shoulders-hip)
        mslice = self.mesh.section(plane_origin=hip, plane_normal=norm)
        return select_closest(mslice, hip).to_planar()[0].length
    
    @apply_scale
    def __get_arm_length(self):
        arm = 0
        for side in ['LEFT', 'RIGHT']:
            shoulder = self.position[f'{side}_SHOULDER'].copy()
            shoulder[0] *= 1.1

            elbow = self.position[f'{side}_ELBOW'].copy()
            elbow[0] *= 1.1

            wrist = self.position[f'{side}_WRIST'].copy()
            wrist[0] *= 1.1

            start, middle, end = self.proximity.vertex([shoulder, elbow, wrist])[-1]
            
            path1 = shortest_path(self.wg, source=start, target=middle, weight='length')
            path2 = shortest_path(self.wg, source=middle, target=end, weight='length')
            path1_visual = trimesh.load_path(self.mesh.vertices[path1])
            path2_visual = trimesh.load_path(self.mesh.vertices[path2])
            arm += 0.5 * (path1_visual.length + path2_visual.length)
        return arm

    @apply_scale
    def __get_trunk_length(self, isBack:bool=False):
        neck = self.position['NECK'].copy()
        waist = self.position['WAIST'].copy()
        
        neck[-1] = abs(neck[-1]) * 1.1 if not isBack else abs(neck[-1]) * -1.1
        waist[-1] = abs(waist[-1]) * 20 if not isBack else abs(waist[-1]) * -20

        start, end = self.proximity.vertex([neck, waist])[-1]
        
        path1 = shortest_path(self.wg, source=start, target=end, weight='length')
        path1_visual = trimesh.load_path(self.mesh.vertices[path1])
        return path1_visual.length

    @apply_scale
    def __get_trunk_width(self, isBack:bool=False):
        ls = self.position['LEFT_SHOULDER'].copy()
        rs = self.position['RIGHT_SHOULDER'].copy()

        ls[-1] = abs(ls[-1]) * 20 if not isBack else abs(ls[-1]) * -20
        rs[-1] = abs(ls[-1]) * 20 if not isBack else abs(ls[-1]) * -20

        start, end = self.proximity.vertex([ls, rs])[-1]
        
        path1 = shortest_path(self.wg, source=start, target=end, weight='length')
        path1_visual = trimesh.load_path(self.mesh.vertices[path1])
        return path1_visual.length

    @apply_scale
    def __get_waist_to_hip(self):
        waist2hip = 0
        for side in ['LEFT', 'RIGHT']:
            hip = self.position[f'{side}_HIP'].copy()
            hip[0] *= 1.1

            waist = self.position['WAIST'].copy()
            waist[0] = hip[0]

            start, end = self.proximity.vertex([waist, hip])[-1]
            
            path1 = shortest_path(self.wg, source=start, target=end, weight='length')
            path1_visual = trimesh.load_path(self.mesh.vertices[path1])
            waist2hip += 0.5 * path1_visual.length
        return waist2hip

    @apply_scale
    def __get_waist_to_knee(self):
        waist2knee = 0
        for side in ['LEFT', 'RIGHT']:
            hip = self.position[f'{side}_HIP'].copy()
            hip[0] *= 1.1

            knee = self.position[f'{side}_KNEE'].copy()
            knee[0] *= 1.1

            waist = self.position['WAIST'].copy()
            waist[0] = hip[0]

            start, middle, end = self.proximity.vertex([waist, hip, knee])[-1]
            
            path1 = shortest_path(self.wg, source=start, target=middle, weight='length')
            path2 = shortest_path(self.wg, source=middle, target=end, weight='length')
            path1_visual = trimesh.load_path(self.mesh.vertices[path1])
            path2_visual = trimesh.load_path(self.mesh.vertices[path2])
            waist2knee += 0.5 * (path1_visual.length + path2_visual.length)
        return waist2knee

    @apply_scale
    def __get_bicep_girth(self):
        bicep = 0
        for side in ['LEFT', 'RIGHT']:
            elbow = self.position[f'{side}_ELBOW']
            shoulder = self.position[f'{side}_SHOULDER']
            
            dist = np.linalg.norm(shoulder-elbow)
            norm = (shoulder-elbow)/dist
            
            steps = np.linspace(0, dist, 500)

            mslices = self.mesh.section_multiplane(plane_origin=elbow, plane_normal=norm, heights=steps)
            mask = np.array([len(mslice.entities) > 2 for mslice in mslices])
            mslices = [select_closest(mslice, np.array([0,0])) for mslice in np.array(mslices)[mask]]
            idx_max = np.argmax([mslice.length for mslice in mslices])
            self.position[f'{side}_BICEP'] = elbow + norm * steps[mask][idx_max]
            bicep += mslices[idx_max].length * .5
        return bicep

    @apply_scale
    def __get_elbow_girth(self):
        elbow = 0
        for side in ['LEFT', 'RIGHT']:
            pos1 = self.position[f'{side}_ELBOW']
            pos2 = self.position[f'{side}_SHOULDER']
            norm = (pos1-pos2)/np.linalg.norm(pos1-pos2)
            mslice = self.mesh.section(plane_origin=pos1, plane_normal=norm)
            elbow += 0.5 * select_closest(mslice, pos1).to_planar()[0].length
        return elbow

    @apply_scale
    def __get_wrist_girth(self):
        wrist = 0
        for side in ['LEFT', 'RIGHT']:
            pos1 = self.position[f'{side}_WRIST']
            pos2 = self.position[f'{side}_ELBOW']
            norm = pos2 - pos1
            mslice = self.mesh.section(plane_origin=pos1+norm*0.01, plane_normal=norm)
            wrist += 0.5 * select_closest(mslice, pos1).to_planar()[0].length
        return wrist

    @apply_scale
    def __get_knee_girth(self):
        knee = 0
        for side in ['LEFT', 'RIGHT']:
            pos = self.position[f'{side}_KNEE']
            mslice = self.mesh.section(plane_origin=pos, plane_normal=[0,1,0])
            knee += 0.5 * select_closest(mslice, pos).to_planar()[0].length
        return knee

    @apply_scale
    def __get_calf_girth(self):
        calf = 0
        for side in ['LEFT', 'RIGHT']:
            knee = self.position[f'{side}_KNEE']
            ankle = self.position[f'{side}_ANKLE']
            
            dist = np.linalg.norm(knee-ankle)
            norm = (knee-ankle)/dist
            steps = np.linspace(0, dist, 500)

            mslices = self.mesh.section_multiplane(plane_origin=ankle, plane_normal=norm, heights=steps)
            mslices = [select_closest(mslice, np.array([0,0])) for mslice in mslices]
            idx_max = np.argmax([mslice.length for mslice in mslices])
            self.position[f'{side}_CALF'] = ankle + norm * steps[idx_max]
            calf += mslices[idx_max].length * .5
        return calf

    @apply_scale
    def __get_ankle_girth(self):
        ankle = 0
        for side in ['LEFT', 'RIGHT']:
            knee = self.position[f'{side}_KNEE']
            pos = self.position[f'{side}_ANKLE']
            dist = np.linalg.norm(knee-pos)
            norm = (knee-pos)/dist
            
            mslice = self.mesh.section(plane_origin=pos+dist*0.05, plane_normal=norm)
            ankle += select_closest(mslice, pos).to_planar()[0].length * .5
        return ankle

    @apply_scale
    def __get_hip_to_ankle(self):
        leg = 0
        for side in ['LEFT', 'RIGHT']:
            hip = self.position[f'{side}_HIP'].copy()
            ankle = self.position[f'{side}_ANKLE'].copy()
            hip[0] *= 1.1
            ankle[0] *= 1.1

            start, end = self.proximity.vertex([hip, ankle])[-1]
            
            path1 = shortest_path(self.wg, source=start, target=end, weight='length')
            path1_visual = trimesh.load_path(self.mesh.vertices[path1])
            leg += 0.5 * path1_visual.length
        return leg

    @apply_scale
    def __get_waist_to_ankle(self):
        leg = 0
        for side in ['LEFT', 'RIGHT']:
            ankle = self.position[f'{side}_ANKLE'].copy()
            ankle[0] *= 1.1

            waist = self.position[f'WAIST'].copy()
            waist[0] = waist[0]*.4 + ankle[0]*.6

            start, end = self.proximity.vertex([waist, ankle])[-1]
            
            path1 = shortest_path(self.wg, source=start, target=end, weight='length')
            path1_visual = trimesh.load_path(self.mesh.vertices[path1])
            leg += 0.5 * path1_visual.length
        return leg

    def to_dict(self):
        return {'neck_girth':self.neck_girth,
                'soulder_length':self.soulder_length,
                'bust_girth':self.bust_girth,
                'waist_girth':self.waist_girth,
                'hip_girth':self.hip_girth,
                'arm_length':self.arm_length,
                'front_length':self.front_length,
                'front_width':self.front_width,
                'back_length':self.back_length,
                'back_width':self.back_width,
                'waist_to_hip':self.waist_to_hip,
                'waist_to_knee':self.waist_to_knee,
                'waist_to_ankle':self.waist_to_ankle,
                'hip_to_ankle':self.hip_to_ankle,
                'bicep_girth':self.bicep_girth,
                'elbow_girth':self.elbow_girth,
                'wrist_girth':self.wrist_girth,
                'knee_girth':self.knee_girth,
                'calf_girth':self.calf_girth,
                'ankle_girth':self.ankle_girth}