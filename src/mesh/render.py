#%%
import trimesh
import numpy as np

from tqdm import tqdm

#%%
def unit_vector(v:np.ndarray, axis:int=1):
    return (v.T/np.linalg.norm(v, axis=axis)).T

def ray_color(mesh, ray_origin, ray_direction):  
    location, index_ray, index_tri = mesh.ray.intersects_location(ray_origin, ray_direction, multiple_hits=False)
    
    colors = np.zeros((len(ray_origin), 3))
    colors[index_ray] += 0.5 * (mesh.face_normals[index_tri] + np.ones_like(mesh.face_normals[index_tri]))
    index_nothitted = np.ones(len(ray_origin), dtype=bool)
    index_nothitted[index_ray] = False
    index_nothitted = np.where(index_nothitted)[0]
    colors[index_nothitted] = np.array([1,1,1])
    return colors

def rendering(mesh, pixels, n_rays=None):
    mesh = mesh.copy()
    mesh.apply_translation(np.diff(mesh.bounds, axis=0)[0]/2 - mesh.bounds[1,:])
    n_rays = pixels if n_rays is None else n_rays
    nx, ny = pixels, pixels
    b, t = mesh.bounds[:, np.argmax(np.diff(mesh.bounds, axis=0))]

    look_from = np.array([0,0,mesh.bounds[:,2].max()]) # camera position
    look_at = np.array([0,0,-1])
    vup = np.array([0,1,0])

    w = unit_vector((look_from - look_at), axis=0)
    vu = unit_vector(np.cross(vup, w), axis=0)
    vv = unit_vector(np.cross(w, vu), axis=0)

    array = np.zeros([ny, nx, 3])
    for _ in tqdm(range(n_rays)):
        u = b + (t-b)*(np.arange(nx)+np.random.rand(nx)+0.5)/nx
        v = b + (t-b)*(np.arange(ny, 0, -1)+np.random.rand(ny)+0.5)/ny
        U, V = np.meshgrid(u, v)
        U, V = U.reshape(-1,1), V.reshape(-1,1)

        ray_origin = look_from + U * vu + V * vv
        ray_direction = np.array([-w]*len(ray_origin))
        array += ray_color(mesh, ray_origin, ray_direction).reshape(nx,ny,3)
    
    del mesh
    frame = (255*np.clip(np.sqrt(array/n_rays), 0, 1)).astype(np.uint8)
    return frame

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    mesh = trimesh.primitives.Sphere(radius=3, center=(0,0,0), subdivisions=3)
    img = rendering(mesh, 500)
    plt.imshow(img)
    plt.show()