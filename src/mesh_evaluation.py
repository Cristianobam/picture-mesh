import sys
sys.path.insert(0, '..')
import os
import cv2
import json
import torch
import base64
import trimesh
import numpy as np

from datetime import datetime
from bodyfit.bodyfat import BF
from mesh.render import rendering
from mesh.wardrobe import Wardrobe
from mesh.utils import drop_artefacts
from bodyfit.somatotype import somatotype
from src.image_processing import image_offset

def mesh_evaluation(mesh, height, gender, age, weight, output_path=f'..{os.sep}results', debug=True):
    try: os.mkdir(output_path)
    except: pass
    
    n_rays = 100
    try:
        import pyembree
    except:
        n_rays = 10
    
    gender = 'female' if gender.lower().startswith('f') else 'male'
    mesh.apply_translation(np.diff(mesh.bounds, axis=0)[0]/2 - mesh.bounds[1,:])
    front_mesh = mesh.copy()
    back_mesh = mesh.copy()

    image_rendered = rendering(mesh=front_mesh, pixels=600, n_rays=n_rays)
    if debug: cv2.imwrite('front_rendered.jpg', image_rendered)

    R = trimesh.transformations.rotation_matrix(np.pi, [0,1,0])
    back_mesh.apply_transform(R)
    image_rendered_back = rendering(mesh=back_mesh, pixels=600, n_rays=n_rays)
    if debug: 
        cv2.imwrite('back_rendered.jpg', image_rendered_back)
        front_mesh.export('mesh.obj')
    
    wardrobe = Wardrobe(mesh=front_mesh, image=image_rendered, height=height)
    wardrobe_results = wardrobe.to_dict()

    wardrobe_results['front_image'] = base64.b64encode(cv2.imencode('.jpg', image_offset(image_rendered))[-1]).decode()
    wardrobe_results['back_image'] = base64.b64encode(cv2.imencode('.jpg', image_offset(image_rendered_back))[-1]).decode()

    wardrobe_results['summary'] = {}
    wardrobe_results['summary']['body_fat'] = BF[gender](wardrobe_results['waist_girth'], wardrobe_results['neck_girth'], wardrobe_results['hip_girth'], height)
    
    bottom_trunk, upper_trunk = getTrunkParts(wardrobe)
    wardrobe_results['summary']['somatotype'] = somatotype(bottom_trunk, upper_trunk, age, height, weight, gender, wardrobe_results['wrist_girth'])[-1]
    
    DBF = {'female':0.225,'male':0.155}[gender]
    wardrobe_results['summary']['ideal_weight'] = (weight * (1 - wardrobe_results['summary']['body_fat']/100))/(1-DBF)
    
    name = datetime.now().strftime('%Y%m%d-%H%M%S')
    front_mesh.export(f'{output_path}{os.sep}{name}.obj')
    cv2.imwrite(f'{output_path}{os.sep}{name}_rendered_front.jpg', image_rendered)
    cv2.imwrite(f'{output_path}{os.sep}{name}_rendered_back.jpg', image_rendered_back)
    with open(f'{output_path}{os.sep}{name}.json', 'w') as fjson:
        json.dump(wardrobe_results, fjson)
    
    if torch.cuda.is_available(): torch.cuda.empty_cache()
    del front_mesh, back_mesh
    return json.dumps(wardrobe_results), name

def getTrunkParts(wardrobe, n=10000):
    dirRight = wardrobe.position['RIGHT_ELBOW'] - wardrobe.position['RIGHT_SHOULDER']
    pointsRight = np.array(list(map(lambda x: wardrobe.position['RIGHT_SHOULDER'] + dirRight * x, np.linspace(0, 1, n))))

    dirLeft = wardrobe.position['LEFT_ELBOW'] - wardrobe.position['LEFT_SHOULDER']
    pointsLeft = np.array(list(map(lambda x: wardrobe.position['LEFT_SHOULDER'] + dirLeft * x, np.linspace(0, 1, n))))
    
    mask1 = wardrobe.mesh.vertices[:,0] < wardrobe.position['RIGHT_SHOULDER'][0]
    distRight = np.array(list(map(lambda vertex: np.linalg.norm(pointsRight-vertex, axis=1).min(), wardrobe.mesh.vertices[mask1])))
    mask1[mask1 == True] = distRight < distRight.mean() * 0.25

    mask2 = wardrobe.mesh.vertices[:,0] > wardrobe.position['LEFT_SHOULDER'][0]
    distLeft = np.array(list(map(lambda vertex: np.linalg.norm(pointsLeft-vertex, axis=1).min(), wardrobe.mesh.vertices[mask2])))
    mask2[mask2 == True] = distLeft < distLeft.mean() * 0.25

    mask = np.logical_or(mask1, mask2)

    maskSlice = np.isin(wardrobe.mesh.faces, np.where(np.logical_not(mask))[0]).all(axis=1)
    meshCopy = wardrobe.mesh.copy()
    meshCopy.update_faces(maskSlice)
    meshCopy.remove_unreferenced_vertices()

    meshCopy = drop_artefacts(meshCopy)

    poship = np.mean([wardrobe.position['LEFT_HIP'], wardrobe.position['RIGHT_HIP']], axis=0)
    posshoulder = np.mean([wardrobe.position['LEFT_SHOULDER'], wardrobe.position['RIGHT_SHOULDER']], axis=0)
    posneck = wardrobe.position['NECK']
    poswaist = wardrobe.position['WAIST']

    meshCopy = trimesh.intersections.slice_mesh_plane(meshCopy, plane_normal=posshoulder - poship, plane_origin=poship)
    meshCopy = trimesh.intersections.slice_mesh_plane(meshCopy, plane_normal=posshoulder - posneck, plane_origin=0.75*posneck+0.25*posshoulder)
    upperTrunk = trimesh.intersections.slice_mesh_plane(meshCopy, plane_normal=posshoulder - poswaist, plane_origin=poswaist)
    bottomTrunk = trimesh.intersections.slice_mesh_plane(meshCopy, plane_normal=-(posshoulder - poswaist), plane_origin=poswaist)

    upperTrunk.apply_scale(wardrobe.mesh2height)
    bottomTrunk.apply_scale(wardrobe.mesh2height)
    return bottomTrunk, upperTrunk