#%% Import Packages
import os
import cv2
import torch
import argparse
import numpy as np
import torchvision.transforms as transforms

from mediapipe.python.solutions import pose as mp_pose
from datetime import datetime

#%% Run ML
def person_focus(image, output_path:str=None, BG_COLOR=(192, 192, 192)):
  with mp_pose.Pose(static_image_mode=True,
                    model_complexity=2,
                    enable_segmentation=True,
                    min_detection_confidence=0.85) as pose:
    
    results = pose.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

    if not results.pose_landmarks:
      raise 'No person detected. Please, feed other image.'
     
    annotated_image = image.copy()

    condition = np.stack((results.segmentation_mask,) * 3, axis=-1) > 0.45
    bg_image = np.zeros(image.shape, dtype=np.uint8)
    bg_image[:] = BG_COLOR
    annotated_image = np.where(condition, annotated_image, bg_image)

    mask = condition.mean(axis=-1)
    rows, cols = np.max(mask, axis=1), np.max(mask, axis=0)
    row_low = int(rows.argmax()*0.8)
    row_high = int(-rows[::-1].argmax()*0.8)
    col_low = int(cols.argmax()*0.8)
    col_high = int(-cols[::-1].argmax()*0.8)
    row_high = -1 if row_high == 0 else row_high
    col_high = -1 if col_high == 0 else col_high
    
    annotated_image = annotated_image[row_low:row_high, col_low:col_high]
    if output_path is not None:
        cv2.imwrite(datetime.now())
    return annotated_image

def image_offset(img, aspect=16/9, BG_COLOR=[255, 255, 255]):
    img_new = person_focus(img, BG_COLOR=BG_COLOR)
    h_new, w_new = img_new.shape[:2]
    h_offset = int((w_new*aspect)-h_new)//2
    w_offset = int((h_new*1/aspect)-w_new)//2
    if w_offset < 0:
        return cv2.copyMakeBorder(img_new, h_offset, h_offset, 0, 0, cv2.BORDER_CONSTANT, value=BG_COLOR)
    else:
        return cv2.copyMakeBorder(img_new, 0, 0, w_offset, w_offset, cv2.BORDER_CONSTANT, value=BG_COLOR)

def image_centering(img, rect):
    x, y, w, h = rect

    left = abs(x) if x < 0 else 0
    top = abs(y) if y < 0 else 0
    right = abs(img.shape[1]-(x+w)) if x + w >= img.shape[1] else 0
    bottom = abs(img.shape[0]-(y+h)) if y + h >= img.shape[0] else 0
    
    if img.shape[2] == 4:
        color = [0, 0, 0, 0]
    else:
        color = [0, 0, 0]
    new_img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)

    x = x + left
    y = y + top
    
    return new_img[y:(y+h),x:(x+w),:]

class EvalImage():
    def __init__(self, image:np.ndarray, loadSize:int=1024):
        self.img = person_focus(image)
        self.loadSize = loadSize
        self.to_tensor = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])  
        self.initialize()

    @classmethod
    def from_path(cls, image_path:str, loadsize=1024):
        assert image_path.lower().endswith(('.jpg', '.jpeg', '.png')), 'Make sure image is one of PNG, JPEG or JPG.'
        img = cv2.imread(image_path, cv2.IMREAD_COLOR)
        return cls(img, loadsize)

    def initialize(self):
        img = self.img.copy()
        if img.shape[2] == 4:
            img = img / 255.0
            img[:,:,:3] /= img[:,:,3:] + 1e-8
            img = img[:,:,3:] * img[:,:,:3] + 0.5 * (1.0 - img[:,:,3:])
            img = (255.0 * img).astype(np.uint8)
        h, w = img.shape[:2]
        
        intrinsic = np.identity(4)
        trans_mat = np.identity(4) # Translation Matrix

        y, x = np.array(img.shape[:2])//2
        radius = max([x, y])
        rect = np.array([x-radius, y-radius, radius*2, radius*2])

        img = image_centering(img, rect)

        scale_im2ndc = 1.0 / float(w // 2)
        scale = w / rect[2]
        trans_mat *= scale
        trans_mat[3,3] = 1.0
        trans_mat[0, 3] = -scale*(rect[0] + rect[2]//2 - w//2) * scale_im2ndc
        trans_mat[1, 3] = scale*(rect[1] + rect[3]//2 - h//2) * scale_im2ndc
        
        intrinsic = np.matmul(trans_mat, intrinsic)
        im_512 = cv2.resize(img, (512, 512))
        img = cv2.resize(img, (self.loadSize, self.loadSize))

        image_512 = cv2.cvtColor(im_512, cv2.COLOR_BGR2RGB)
        image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        B_MIN = np.array([-1, -1, -1])
        B_MAX = np.array([1, 1, 1])
        projection_matrix = np.identity(4)
        projection_matrix[1, 1] = -1
        
        calib = torch.Tensor(projection_matrix).float()
        calib_world = torch.Tensor(intrinsic).float()

        # image
        image_512 = self.to_tensor(image_512)
        image = self.to_tensor(image)
        
        self.__dict = {
            'img': image.unsqueeze(0),
            'img_512': image_512.unsqueeze(0),
            'calib': calib.unsqueeze(0),
            'calib_world': calib_world.unsqueeze(0),
            'b_min': B_MIN,
            'b_max': B_MAX,
      }
        
    @property
    def dict(self):
        return self.__dict

# %%
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--image_path', type=str, required=True)
    parser.add_argument('-o', '--output_path', type=str, default=None, required=False)
    args = parser.parse_args()
    person_focus(file_path=args.image_path, output_path=args.output_path)