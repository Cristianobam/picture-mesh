import sys
sys.path.insert(0, '..')
import trimesh
import numpy as np
import json

from bodyfit.utils import *

def getTrunkIndex(bottom_trunk:trimesh.base.Trimesh, top_trunk:trimesh.base.Trimesh) -> float:
    return roundcap(float(top_trunk.volume / bottom_trunk.volume), 0.05, 2)

def getWristIndex(wrist_girth, height):
    return wrist_girth / height*100

def somatotype(bottom_trunk, upper_trunk, age, height, weight, gender, wrist_girth):
    genderS = 'female' if gender.lower().startswith('f') else 'male'
    heightS = centimeter2inches(height)
    weightS = kilogram2pound(weight)
    wrist_girthS = centimeter2inches(wrist_girth)
    trunkIndex = getTrunkIndex(bottom_trunk, upper_trunk)
    wristIndex = getWristIndex(wrist_girth, height)
    
    if genderS == 'female':
        with open('bodyfit/female-somatotype.json', 'r') as fjson:
            somatotypeTable = json.load(fjson)
    else:
        with open('bodyfit/male-somatotype.json', 'r') as fjson:
            somatotypeTable = json.load(fjson)
    
    somatotypeTable = {k:np.array(v) for k,v in zip(somatotypeTable.keys(),somatotypeTable.values())} 
    
    with open('bodyfit/wristHeightIndex.json', 'r') as fjson:
        wristTable = json.load(fjson)
    
    if ~np.isin(heightS, somatotypeTable['HEIGHT INCHES']):
        indexHeight = somatotypeTable['HEIGHT INCHES']==roundcap(heightS, 1, 1)
    else:
        indexHeight = somatotypeTable['HEIGHT INCHES']==heightS
    
    if ~np.isin(trunkIndex, somatotypeTable['TRUNK INDEX']):
        if ~np.isin(trunkIndex := roundcap(trunkIndex, 0.1, 2), somatotypeTable['TRUNK INDEX']):
            indexTrunk = somatotypeTable['TRUNK INDEX']==roundcap(trunkIndex, 1, 1)
        else:
            indexTrunk = somatotypeTable['TRUNK INDEX']==trunkIndex
    else:
        indexTrunk = somatotypeTable['TRUNK INDEX']==trunkIndex
    
    if 20 <= age < 30:
        indexWeight = somatotypeTable['MAX LBS @20']==roundcap(weightS, 1, 1)
        table = np.c_[somatotypeTable['HEIGHT INCHES'], somatotypeTable['TRUNK INDEX'], somatotypeTable['MAX LBS @20']]
    elif 30 <= age < 40:
        indexWeight = somatotypeTable['MAX LBS @30']==roundcap(weightS, 1, 1)
        table = np.c_[somatotypeTable['HEIGHT INCHES'], somatotypeTable['TRUNK INDEX'], somatotypeTable['MAX LBS @30']]
    elif 40 <= age < 50:
        indexWeight = somatotypeTable['MAX LBS @40']==roundcap(weightS, 1, 1)
        table = np.c_[somatotypeTable['HEIGHT INCHES'], somatotypeTable['TRUNK INDEX'], somatotypeTable['MAX LBS @40']]
    elif age > 50:
        indexWeight = somatotypeTable['MAX LBS @50']==roundcap(weightS, 1, 1)
        table = np.c_[somatotypeTable['HEIGHT INCHES'], somatotypeTable['TRUNK INDEX'], somatotypeTable['MAX LBS @50']]
    

    if any(index := indexHeight & indexTrunk & indexWeight):
        indexN = 0
    elif any(index := indexHeight & indexTrunk):
        indexN = np.argmin(np.linalg.norm(table[index]-np.array([heightS, trunkIndex, weightS]), axis=1))
    elif any(index := indexHeight & indexWeight):
        indexN = np.argmin(np.linalg.norm(table[index]-np.array([heightS, trunkIndex, weightS]), axis=1))
    elif any(index := indexTrunk & indexWeight):
        indexN = np.argmin(np.linalg.norm(table[index]-np.array([heightS, trunkIndex, weightS]), axis=1))
    elif any(index := indexHeight):
        indexN = np.argmin(np.linalg.norm(table[index]-np.array([heightS, trunkIndex, weightS]), axis=1))
    elif any(index := indexWeight):
        indexN = np.argmin(np.linalg.norm(table[index]-np.array([heightS, trunkIndex, weightS]), axis=1))
    elif any(index := indexTrunk):
        indexN = np.argmin(np.linalg.norm(table[index]-np.array([heightS, trunkIndex, weightS]), axis=1))
    else:
        return None, 'Indefinido'

    results = somatotypeTable['ENDO'][index][indexN], \
            somatotypeTable['MESO'][index][indexN], \
            somatotypeTable['ECTO'][index][indexN], \
            somatotypeTable['BALANCE'][index][indexN]

    return results, ['Endomorfo', 'Mesomorfo', 'Ectomorfo'][np.argmax(results[:-1])]