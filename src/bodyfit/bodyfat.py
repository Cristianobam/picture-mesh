import numpy as np

BF = {'male': lambda waist, neck, hip, height: np.clip(495/(1.0324-0.19077*np.log10(waist-neck)+0.15456*np.log10(height))-450, a_min=0, a_max=100),
     'female': lambda waist, neck, hip, height: np.clip(495/(1.29579-0.35004*np.log10(waist+hip-neck)+0.22100*np.log10(height))-450, a_min=0, a_max=100)}